# Migrations

> A Java program to convert CSVs exported from MySQL databases to JSONs that can
> be imported in MongoDB.

## :rocket: Getting Started

### :hammer: Development

Compile and run the project in a clean build.

```
mvn clean compile exec:java
```

Running tests.

```
mvn test
```

Format the code accordingly to common guide lines.

```
mvn formatter:format
```

Lint your code with _checkstyle_.

```
mvn checkstyle:check
```

### :package: Deployment

Bundling the app into jar file.

```
mvn package
```
