import com.opencsv.exceptions.CsvValidationException;
import model.mysql.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.Parser;

public final class App {
    private App() {

    }
    private static void writeToFile(FileWriter file, Map<Integer, toJSON> map,List<Map<String,String>> ... tables) throws Exception
    {
        file.write("[");
        file.flush();
        for (int i=1;i<=map.size();i++)
            if(i<map.size())
                map.get(i).writeToJSON(file,true,tables);
            else map.get(i).writeToJSON(file,false,tables);
        file.write("]");
        file.flush();
        file.close();
    }
    public static void main(final String[] args) {
        try {
            List<Map<String, String>> fileAtleta = Parser.readCSV("data/Atleta.csv");
            List<Map<String,String>> fileModalidade = Parser.readCSV("data/Modalidade.csv");
            List<Map<String,String>> fileAtlInProva = Parser.readCSV("data/Atleta_In_Prova.csv");
            List<Map<String,String>> fileConsultas = Parser.readCSV("data/Consulta.csv");
            List<Map<String,String>> fileExInConsulta = Parser.readCSV("data/Exame_In_Consulta.csv");
            List<Map<String,String>> fileExames = Parser.readCSV("data/Exame.csv");
            List<Map<String,String>> fileProva = Parser.readCSV("data/Prova.csv");
            List<Map<String,String>> fileOrganizador = Parser.readCSV("data/Organizador.csv");
            List<Map<String,String>> fileEqInExame = Parser.readCSV("data/Equipamento_In_Exame.csv");
            List<Map<String,String>> fileEqInHospital = Parser.readCSV("data/Equipamento_In_Hospital.csv");
            List<Map<String,String>> fileEquipamentos = Parser.readCSV("data/Equipamento.csv");
            List<Map<String,String>> fileExsInProva = Parser.readCSV("data/ExamesNeededInProva.csv");
            List<Map<String,String>> fileHospital = Parser.readCSV("data/Hospital.csv");
            List<Map<String,String>> filePedido = Parser.readCSV("data/Pedidos_Exame.csv");
            List<Map<String,String>> fileMedico = Parser.readCSV("data/Medico.csv");
            List<Map<String,String>> fileEspecialidade = Parser.readCSV("data/Especialidade.csv");
            Map<Integer,toJSON> atletas = new HashMap<>();
            Map<Integer, toJSON> modalidades = new HashMap<>();
            Map<Integer, toJSON> consultas = new HashMap<>();
            Map<Integer,toJSON> exames = new HashMap<>();
            Map<Integer,toJSON> provas = new HashMap<>();
            Map<Integer,toJSON> organizadores = new HashMap<>();
            Map<Integer,toJSON> equipamentos = new HashMap<>();
            Map<Integer,toJSON> medicos = new HashMap<>();
            Map<Integer,toJSON> especialidades = new HashMap<>();
            Map<Integer,toJSON> hospitais = new HashMap<>();
            fileAtleta.forEach((map) -> {
                Atleta a = new Atleta(map);
                atletas.put(a.getID(),a);
            });
            fileModalidade.forEach((map)-> {
                Modalidade m = new Modalidade(map);
                modalidades.put(m.getId(),m);
            });
            fileConsultas.forEach((map)-> {
                Consulta c = new Consulta(map);
                consultas.put(c.getId(),c);
            });
            fileExames.forEach((map) -> {
                Exame e = new Exame(map);
                exames.put(e.getId(),e);
            });
            fileProva.forEach((map)-> {
                Prova p = new Prova(map);
                provas.put(p.getId(),p);
            });
            fileOrganizador.forEach((map)->{
                Organizador o = new Organizador(map);
                organizadores.put(o.getId(),o);
            });
            fileEquipamentos.forEach((map)->{
                Equipamento e = new Equipamento(map);
                equipamentos.put(e.getID(),e);
            });
            fileMedico.forEach((map)-> {
                Medico m = new Medico(map);
                medicos.put(m.getId(),m);
            });
            fileEspecialidade.forEach((map)-> {
                Especialidade e = new Especialidade(map);
                especialidades.put(e.getId(),e);
            });
            fileHospital.forEach((map)-> {
                Hospital h = new Hospital(map);
                hospitais.put(h.getId(),h);
            });
            FileWriter fileOutAtletas = new FileWriter("json/atleta.json");
            FileWriter fileOutModalidades = new FileWriter("json/modalidades.json");
            FileWriter fileOutConsultas = new FileWriter("json/consultas.json");
            FileWriter fileOutExames = new FileWriter("json/exames.json");
            FileWriter fileOutProvas = new FileWriter("json/provas.json");
            FileWriter fileOutOrganizadores = new FileWriter("json/organizadores.json");
            FileWriter fileOutEquipamentos = new FileWriter("json/equipamentos.json");
            FileWriter fileOutHospitais = new FileWriter("json/hospitais.json");
            FileWriter fileOutMedicos = new FileWriter("json/medicos.json");
            FileWriter fileOutEspecialidades = new FileWriter("json/especialidades.json");
            writeToFile(fileOutAtletas,atletas,fileAtlInProva);
            writeToFile(fileOutModalidades,modalidades,fileAtleta,fileProva);
            writeToFile(fileOutConsultas,consultas,fileConsultas);
            writeToFile(fileOutExames,exames,fileExInConsulta);
            writeToFile(fileOutProvas,provas,fileExsInProva);
            writeToFile(fileOutOrganizadores,organizadores,fileProva);
            writeToFile(fileOutEquipamentos,equipamentos,fileEqInExame,fileEqInHospital);
            writeToFile(fileOutMedicos,medicos,fileMedico);
            writeToFile(fileOutEspecialidades,especialidades,fileMedico);
            writeToFile(fileOutHospitais,hospitais,filePedido);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
