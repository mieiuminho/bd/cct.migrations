package model.mysql;

import java.util.Map;

public class Atleta_In_Prova {
    private int idAtleta;
    private int idProva;
    public Atleta_In_Prova(Map<String,String> map)
    {
        this.idAtleta=Integer.parseInt(map.get("idAtleta"));
        this.idProva=Integer.parseInt(map.get("idProva"));
    }
    public int getIdAtleta()
    {
        return idAtleta;
    }
    public int getIdProva()
    {
        return idProva;
    }
}
