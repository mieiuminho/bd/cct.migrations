package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Hospital implements toJSON{
    private int id;
    private String nome;
    public Hospital(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.nome = map.get("nome");
    }
    public int getId()
    {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray pedidos = new JSONArray();
        List<Map<String,String>> pedidosExame = tables[0];
        obj.put("id",this.id);
        obj.put("name","" +this.nome);
        for (Map<String,String> map : pedidosExame)
        {
            if (Integer.parseInt(map.get("idHospital")) == this.id)
            {
                JSONObject pedidoToHospital = new JSONObject();
                pedidoToHospital.put("atleta",Integer.parseInt(map.get("idAtleta")));
                pedidoToHospital.put("exame",Integer.parseInt(map.get("idExame")));
                pedidos.add(pedidoToHospital);
            }
        }
        obj.put("pedidos",pedidos);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
