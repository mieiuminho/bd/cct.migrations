package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Medico implements toJSON{
    private int id;
    private String email;
    private String nome;
    private int especialidade_id;
    private int hospital;
    public Medico(Map<String,String> map)
    {
        this.id=Integer.parseInt(map.get("id"));
        this.email=map.get("email");
        this.nome=map.get("nome");
        this.especialidade_id=Integer.parseInt(map.get("especialidade_id"));
        this.hospital=Integer.parseInt(map.get("hospital"));
    }
    public int getId()
    {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        obj.put("id",this.id);
        obj.put("email","" +this.email);
        obj.put("nome",""+this.nome);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
