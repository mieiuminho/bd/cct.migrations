package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class Consulta implements toJSON{
    private int id;
    private int idMedico;
    private int idAtleta;
    private LocalDateTime hora_inicio;
    private LocalDateTime hora_fim;
    public Consulta(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.idMedico = Integer.parseInt(map.get("idMedico"));
        this.idAtleta = Integer.parseInt(map.get("idAtleta"));
        String [] dataHoraI = map.get("hora_inicio").split(" ");
        String [] dataI = dataHoraI[0].split("-");
        String [] horaI = dataHoraI[1].split(":");
        String [] dataHoraF = map.get("hora_fim").split(" ");
        String [] dataF = dataHoraF[0].split("-");
        String [] horaF = dataHoraF[1].split(":");
        this.hora_inicio = LocalDateTime.of(Integer.parseInt(dataI[0]),Integer.parseInt(dataI[1]),Integer.parseInt(dataI[2]),
            Integer.parseInt(horaI[0]),Integer.parseInt(horaI[1]),Integer.parseInt(horaI[2]));
        this.hora_fim = LocalDateTime.of(Integer.parseInt(dataF[0]),Integer.parseInt(dataF[1]),Integer.parseInt(dataF[2]),
            Integer.parseInt(horaF[0]),Integer.parseInt(horaF[1]),Integer.parseInt(horaF[2]));
    }
    public int getId()
    {
        return id;
    }
    public int getIdMedico()
    {
        return idMedico;
    }
    public int getIdAtleta()
    {
        return idAtleta;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray provas = new JSONArray();
        List<Map<String,String>> atletasInProva = tables[0];
        obj.put("id",this.id);
        obj.put("idMedico",this.idMedico);
        obj.put("idAtleta",this.idAtleta);
        obj.put("hora_inicio",""+this.hora_inicio);
        obj.put("hora_fim",""+this.hora_fim);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
