package model.mysql;

import java.util.Map;

public class Pedidos_Exame {
    private int idAtleta;
    private int idHospital;
    private int idExame;
    public Pedidos_Exame(Map<String,String> map)
    {
        this.idAtleta = Integer.parseInt(map.get("idAtleta"));
        this.idHospital = Integer.parseInt(map.get("idHospital"));
        this.idExame = Integer.parseInt(map.get("idExame"));
    }

    public int getIdAtleta() {
        return idAtleta;
    }

    public int getIdExame() {
        return idExame;
    }

    public int getIdHospital() {
        return idHospital;
    }
}
