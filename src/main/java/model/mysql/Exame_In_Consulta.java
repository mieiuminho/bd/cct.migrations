package model.mysql;

import java.util.Map;

public class Exame_In_Consulta {
    private int exame_id;
    private int consulta_id;
    public Exame_In_Consulta(Map<String,String> map)
    {
        this.exame_id=Integer.parseInt(map.get("exame_id"));
        this.consulta_id=Integer.parseInt(map.get("consulta_id"));
    }
    public int getExame_id()
    {
        return exame_id;
    }
    public int getConsulta_id()
    {
        return consulta_id;
    }
}
