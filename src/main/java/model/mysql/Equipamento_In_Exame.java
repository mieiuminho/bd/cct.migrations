package model.mysql;

import java.util.Map;

public class Equipamento_In_Exame {
    private int idEquipamento;
    private int idExame;
    public Equipamento_In_Exame(Map<String,String> map)
    {
        this.idEquipamento = Integer.parseInt(map.get("idEquipamento"));
        this.idExame = Integer.parseInt(map.get("idExame"));
    }
    public int getIdEquipamento()
    {
        return idEquipamento;
    }
    public int getIdExame()
    {
        return idExame;
    }
}
