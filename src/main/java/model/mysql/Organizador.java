package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Organizador implements toJSON{
    private int id;
    private String nome;
    private String email;
    public Organizador(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.nome = map.get("nome");
        this.email = map.get("email");
    }

    public int getId() {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray provas = new JSONArray();
        List<Map<String,String>> provasTodas = tables[0];
        obj.put("id",this.id);
        obj.put("email","" +this.email);
        obj.put("name","" +this.nome);
        for (Map<String,String> map : provasTodas)
        {
            if (Integer.parseInt(map.get("idOrganizador")) == this.id)
                provas.add(Integer.parseInt(map.get("id")));
        }
        obj.put("provas",provas);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
