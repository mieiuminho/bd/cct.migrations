package model.mysql;

import java.util.Map;

public class Equipamento_In_Hospital {
    private int idHospital;
    private int idEquipamento;
    private int stock;
    public Equipamento_In_Hospital(Map<String,String> map)
    {
        this.idHospital = Integer.parseInt(map.get("idHospital"));
        this.idEquipamento = Integer.parseInt(map.get("idEquipamento"));
        this.stock = Integer.parseInt(map.get("stock"));
    }
    public int getIdHospital ()
    {
        return idHospital;
    }
    public int getIdEquipamento()
    {
        return idEquipamento;
    }

}
