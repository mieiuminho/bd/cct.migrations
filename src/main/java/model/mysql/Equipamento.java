package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Equipamento implements toJSON{
    private int id;
    private String designacao;
    private char reutilizavel;
    public Equipamento(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.designacao = map.get("designacao");
        this.reutilizavel = map.get("reutilizavel").charAt(0);
    }
    public int getID()
    {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray exames = new JSONArray();
        JSONArray hospitais = new JSONArray();
        List<Map<String,String>> equipamentoInExame = tables[0];
        List<Map<String,String>> equipamentoInHospital = tables[1];
        obj.put("id",this.id);
        obj.put("designacao","" +this.designacao);
        for (Map<String,String> map : equipamentoInExame)
        {
            if (Integer.parseInt(map.get("idEquipamento")) == this.id)
                exames.add(Integer.parseInt(map.get("idEquipamento")));
        }
        for (Map<String,String> map : equipamentoInHospital)
        {
            if (Integer.parseInt(map.get("idEquipamento")) == this.id)
            {
                JSONObject hospitalStock = new JSONObject();
                hospitalStock.put("hospital",Integer.parseInt(map.get("idHospital")));
                hospitalStock.put("stock",Integer.parseInt(map.get("stock")));
                hospitais.add(hospitalStock);
            }
        }
        obj.put("exames",exames);
        obj.put("hospitais",hospitais);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
