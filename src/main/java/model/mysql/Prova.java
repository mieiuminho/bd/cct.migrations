package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class Prova implements toJSON {
    private int id;
    private String nome;
    private int idOrganizador;
    private LocalDateTime data_inicio;
    private LocalDateTime data_fim;
    private int idModalidade;
    public Prova(Map<String,String> map){
        this.id = Integer.parseInt(map.get("id"));
        this.nome = map.get("nome");
        this.idOrganizador = Integer.parseInt(map.get("idOrganizador"));
        String [] dataHoraI = map.get("data_inicio").split(" ");
        String [] dataI = dataHoraI[0].split("-");
        String [] horaI = dataHoraI[1].split(":");
        String [] dataHoraF = map.get("data_fim").split(" ");
        String [] dataF = dataHoraF[0].split("-");
        String [] horaF = dataHoraF[1].split(":");
        this.data_inicio = LocalDateTime.of(Integer.parseInt(dataI[0]),Integer.parseInt(dataI[1]),Integer.parseInt(dataI[2]),
            Integer.parseInt(horaI[0]),Integer.parseInt(horaI[1]),Integer.parseInt(horaI[2]));
        this.data_fim = LocalDateTime.of(Integer.parseInt(dataF[0]),Integer.parseInt(dataF[1]),Integer.parseInt(dataF[2]),
            Integer.parseInt(horaF[0]),Integer.parseInt(horaF[1]),Integer.parseInt(horaF[2]));
        this.idModalidade = Integer.parseInt(map.get("idModalidade"));
    }

    public int getId() {
        return id;
    }

    public int getIdModalidade() {
        return idModalidade;
    }

    public int getIdOrganizador() {
        return idOrganizador;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray exames = new JSONArray();
        List<Map<String,String>> examesInProva = tables[0];
        obj.put("id",this.id);
        obj.put("name","" +this.nome);
        obj.put("data_inicio","" +this.data_inicio);
        obj.put("data_fim",""+this.data_fim);
        for (Map<String,String> map : examesInProva)
        {
            if (Integer.parseInt(map.get("idProva")) == this.id)
            {
                JSONObject exameData = new JSONObject();
                exameData.put("exame",Integer.parseInt(map.get("idExame")));
                exameData.put("data_limite",""+map.get("data_limite"));
                exames.add(exameData);
            }
        }
        obj.put("exames",exames);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
