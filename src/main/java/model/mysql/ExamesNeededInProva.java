package model.mysql;

import java.time.LocalDateTime;
import java.util.Map;

public class ExamesNeededInProva {
    private int idProva;
    private int idExame;
    private LocalDateTime data_limite;
    public ExamesNeededInProva(Map<String,String> map)
    {
        this.idProva = Integer.parseInt(map.get("idProva"));
        this.idExame = Integer.parseInt(map.get("idExame"));
        String [] dataHoraI = map.get("data_limite").split(" ");
        String [] dataI = dataHoraI[0].split("-");
        String [] horaI = dataHoraI[1].split(":");
        this.data_limite = LocalDateTime.of(Integer.parseInt(dataI[0]),Integer.parseInt(dataI[1]),Integer.parseInt(dataI[2]),
            Integer.parseInt(horaI[0]),Integer.parseInt(horaI[1]),Integer.parseInt(horaI[2]));
    }
    public int getIdProva()
    {
        return idProva;
    }
    public int getIdExame()
    {
        return idExame;
    }

}
