package model.mysql;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public interface toJSON {
    public void writeToJSON(FileWriter file, boolean b, List<Map<String,String>> ... tables) throws Exception;
}
