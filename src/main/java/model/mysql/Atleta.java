package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.lang.model.util.SimpleElementVisitor7;
import java.io.FileWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

public final class Atleta implements toJSON{
    private int id;
    private String email;
    private String nome;
    private int telemovel;
    private Date data_nascimento;
    private int altura;
    private int peso;
    private int modalidade_id;
    private char genero;
    public Atleta(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.email = map.get("email");
        this.nome = map.get("nome");
        this.telemovel = Integer.parseInt(map.get("telemovel"));
        String [] auxData = map.get("data_nascimento").split("-");
        this.data_nascimento = new Date(Integer.parseInt(auxData[0]),Integer.parseInt(auxData[1]),Integer.parseInt(auxData[2]));
        this.altura = Integer.parseInt(map.get("altura"));
        this.peso = Integer.parseInt(map.get("peso"));
        this.modalidade_id = Integer.parseInt(map.get("modalidade_id"));
        this.genero= map.get("genero").charAt(0);
    }
    public int getID()
    {
        return this.id;
    }
    public void writeToJSON (FileWriter file,boolean escreveVirgula,List<Map<String,String>> ... tables) throws Exception //Para já só tem o atletasInProva
        //mas depois a ideia é por aqui como argumento todas as tabelas onde atleta esteja presente
    {
        JSONObject obj = new JSONObject();
        JSONArray provas = new JSONArray();
        List<Map<String,String>> atletasInProva = tables[0];
        obj.put("id",this.id);
        obj.put("email","" +this.email);
        obj.put("name","" +this.nome);
        obj.put("telemovel",this.telemovel);
        obj.put("data_nascimento","" +this.data_nascimento);
        obj.put("altura",this.altura);
        obj.put("peso",this.peso);
        //obj.put("modalidade_id","" +this.modalidade_id);
        obj.put("genero","" +this.genero);
        for (Map<String,String> map : atletasInProva)
        {
            if (Integer.parseInt(map.get("idAtleta")) == this.id)
                provas.add(Integer.parseInt(map.get("idProva")));
        }
        obj.put("provas",provas);
        file.write(obj.toJSONString());
        if(escreveVirgula) file.write(",");
        file.flush();
    }
}
