package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Especialidade implements toJSON {
    private int id;
    private String nome;
    public Especialidade(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.nome = map.get("nome");
    }
    public int getId()
    {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray medicos = new JSONArray();
        List<Map<String,String>> medicosTodos = tables[0];
        obj.put("id",this.id);
        obj.put("nome","" +this.nome);
        for (Map<String,String> map : medicosTodos)
        {
            if (Integer.parseInt(map.get("especialidade_id")) == this.id)
                medicos.add(Integer.parseInt(map.get("id")));
        }
        obj.put("medicos",medicos);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
