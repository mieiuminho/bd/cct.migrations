package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Exame implements toJSON{
    private int id;
    private String designacao;
    private int idEspecialidade;
    public Exame(Map<String,String> map)
    {
        this.id=Integer.parseInt(map.get("id"));
        this.designacao=map.get("designacao");
        this.idEspecialidade=Integer.parseInt(map.get("idEspecialidade"));
    }
    public int getId()
    {
        return id;
    }

    @Override
    public void writeToJSON(FileWriter file, boolean b, List<Map<String, String>>... tables) throws Exception {
        JSONObject obj = new JSONObject();
        JSONArray consultas = new JSONArray();
        List<Map<String,String>> exameInConsulta = tables[0];
        obj.put("id",this.id);
        obj.put("designacao","" +this.designacao);
        for (Map<String,String> map : exameInConsulta)
        {
            if (Integer.parseInt(map.get("exame_id")) == this.id)
                consultas.add(Integer.parseInt(map.get("consulta_id")));
        }
        obj.put("consultas",consultas);
        file.write(obj.toJSONString());
        if(b) file.write(",");
        file.flush();
    }
}
