package model.mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class Modalidade implements toJSON {
    private int id;
    private String nome;
    public Modalidade(Map<String,String> map)
    {
        this.id = Integer.parseInt(map.get("id"));
        this.nome = map.get("nome");
    }
    public int getId()
    {
        return id;
    }
    public void writeToJSON(FileWriter file, boolean escreveVirgula,List<Map<String,String>> ... tables) throws Exception
    {
        JSONObject obj = new JSONObject();
        List<Map<String,String>> atletas = tables[0];
        List<Map<String,String>> provas = tables[1];
        obj.put("id",this.id);
        obj.put("nome",this.nome);
        JSONArray atletasInModalidade = new JSONArray();
        JSONArray provasInModalidade = new JSONArray();
        for (Map<String,String> a : atletas)
            if (Integer.parseInt(a.get("modalidade_id")) == this.id)
                atletasInModalidade.add(Integer.parseInt(a.get("id")));
        for (Map<String,String> p : provas)
            if (Integer.parseInt(p.get("idModalidade")) == this.id)
                provasInModalidade.add(Integer.parseInt(p.get("id")));
            obj.put("atletas",atletasInModalidade);
            obj.put("provas",provasInModalidade);
            file.write(obj.toJSONString());
            if (escreveVirgula)
                file.write(",");
            file.flush();
    }
}
