package util;

import com.opencsv.CSVReaderHeaderAware;
import com.opencsv.exceptions.CsvValidationException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class Parser {

    private Parser() {

    }

    public static List<Map<String, String>> readCSV(final String file) throws IOException, CsvValidationException {
        List<Map<String, String>> lines = new ArrayList<>();
        CSVReaderHeaderAware csv = new CSVReaderHeaderAware(new FileReader(file));

        Map<String, String> row;
        while ((row = csv.readMap()) != null) {
            lines.add(row);
        }

        return lines;
    }

    /**
     * Function that reads all lines from a file.
     *
     * @param file from where should read
     * @return List of Strings
     */
    public static List<String> read(final String file) {
        List<String> linhas = new ArrayList<>();
        BufferedReader inFile;
        String linha;

        try {
            inFile = new BufferedReader(new FileReader(file));
            while ((linha = inFile.readLine()) != null) {
                linhas.add(linha);
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return linhas;
    }
}
